import 'package:app_uber/pages/home/home_page.dart';
import 'package:app_uber/pages/login/login_page.dart';
import 'package:app_uber/utils/colors.dart' as custom_colors;
import 'package:flutter/material.dart';

void main() => runApp(const Main());

class Main extends StatelessWidget {
  const Main({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Uber App',
      initialRoute: "home",
      theme: ThemeData(
        fontFamily: "NimbusSans",
        appBarTheme: const AppBarTheme(elevation: 0, 
        backgroundColor: custom_colors.Colors.secondaryColor,
        foregroundColor: custom_colors.Colors.primaryColor,
        surfaceTintColor: custom_colors.Colors.secondaryColor,
        ),
      ),
      routes: {
        "home" : (BuildContext context) => const HomePage(),
        "login" : (BuildContext context) => const LoginPage(),
      },
    );
  }
}