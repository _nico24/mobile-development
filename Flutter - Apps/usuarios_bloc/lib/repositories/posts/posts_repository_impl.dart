// ignore_for_file: depend_on_referenced_packages

import 'package:first_flutter_bloc/models/posts.dart';
import 'package:first_flutter_bloc/repositories/posts/posts_repository.dart';
import 'package:dio/dio.dart' show Dio, Options, Headers;
import '../../utils/constants.dart';

class PostRepositoryImpl extends PostRepository {
  @override
  Future<List<PostModel>> getAllPostsByUser(int userId) async {
    var response = await Dio().get(
      "${Constants.apiUrl}/users/$userId/posts",
      options: Options(
        headers: {
          Headers.contentTypeHeader: "apllication/json",
          Headers.acceptHeader: "application/json",
          "Authorization": "Bearer ${Constants.token}"
        },
      ),
    );

    if (response.statusCode != 200) {
      throw Exception(response.statusMessage);
    }
    return (response.data as List)
        .map((element) => PostModel.fromJson(element))
        .toList();
  }

  @override
  Future<bool> savePostByUser(PostModel postModel) async {
    var response = await Dio().post(
      '${Constants.apiUrl}/users/${postModel.userId}/posts',
      data: postModel.toJson(),
      options: Options(
        headers: {
          Headers.contentTypeHeader: 'application/json',
          Headers.acceptHeader: 'application/json',
          'Authorization': "Bearer ${Constants.token}",
        },
      ),
    );

    if (response.statusCode != 201) {
      throw Exception(response.statusMessage);
    }
    return true;
  }
}
