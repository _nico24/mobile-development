import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:pokemon_v3/models/models.dart';
import '../models/pokemon_description.dart';

class PokemonResponse {
  static Future<AllPokemons> fetchAllPokemon() async {
    final response = await http
        .get(Uri.parse('https://pokeapi.co/api/v2/pokemon?limit=12&offset=0'));
    if (response.statusCode != 200) {
      throw Exception('Failed to load all pokemons');
    }
    return AllPokemons.fromJson(json.decode(response.body));
  }

  static Future<Pokemon> fetchPokemonDetails(String name) async {
    final url = 'https://pokeapi.co/api/v2/pokemon/$name';
    final response = await http.get(Uri.parse(url));
    if (response.statusCode != 200) {
      throw Exception('Failed to load pokemon details');
    }
    return pokemonFromJson(response.body);
  }
}
