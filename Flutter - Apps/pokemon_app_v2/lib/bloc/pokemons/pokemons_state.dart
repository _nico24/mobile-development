part of 'pokemons_bloc.dart';

class PokemonsState extends Equatable {
  final List<AllPokemons> listPokemons;
  const PokemonsState({this.listPokemons = const []});

  PokemonsState copyWith({List<AllPokemons>? listPokemons}) => PokemonsState(
        listPokemons: listPokemons ?? this.listPokemons,
      );

  @override
  List<Object> get props => [listPokemons];
}
