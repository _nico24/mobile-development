part of 'pokemons_bloc.dart';

@immutable
abstract class PokemonsEvent extends Equatable {
  const PokemonsEvent();
}

class GetAllPokemons extends PokemonsEvent {
  @override
  List<Object> get props => [];
}
