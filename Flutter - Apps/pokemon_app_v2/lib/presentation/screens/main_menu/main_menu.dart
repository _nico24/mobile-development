import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:pokemon_v3/presentation/screens/screens.dart';
import '../../../bloc/bottom_navigation/bottom_navigation_bloc.dart';
import '../home/home.dart';

class MainMenu extends StatefulWidget {
  const MainMenu({Key? key}) : super(key: key);

  @override
  State<MainMenu> createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BottomNavigationBloc, int>(
      builder: (context, currentTabIndex) {
        final bottomNavigationBloc = context.read<BottomNavigationBloc>();
        final screens = [
          const HomePokemon(),
          const FavoritePage(),
          const ProfilePage(),
          const SettingsPage(),
        ];
        return Scaffold(
          appBar: AppBar(
              backgroundColor: Colors.black,
              elevation: 5,
              title: const Text("Pokédex",
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w900,
                      fontSize: 30)),
              actions: [
                IconButton(
                    onPressed: () {},
                    icon: const Icon(Icons.notifications,
                        size: 30, color: Colors.white))
              ]),
          backgroundColor: const Color(0x001f1f1f),
          body: IndexedStack(
            index: currentTabIndex,
            children: screens,
          ),
          bottomNavigationBar: BottomNavigationBar(
            elevation: 1,
            currentIndex: currentTabIndex,
            backgroundColor: const Color(0xFF1F1F1F),
            selectedItemColor: const Color(0xFF4BC9FF),
            unselectedItemColor: Colors.white,
            onTap: (index) => bottomNavigationBloc.add(TabChangeEvent(index)),
            type: BottomNavigationBarType.fixed,
            items: const [
              BottomNavigationBarItem(
                icon: Icon(Icons.home),
                label: 'Home',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.favorite),
                label: 'Favoritos',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.person),
                label: 'Perfil',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.settings),
                label: 'Ajustes',
              ),
            ],
          ),
        );
      },
    );
  }
}
