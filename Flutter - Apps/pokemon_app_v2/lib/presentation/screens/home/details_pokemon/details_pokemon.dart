import 'package:flutter/material.dart';
import 'widgets/widgets.dart';

class DetailsPokemon extends StatelessWidget {
  const DetailsPokemon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 5,
          title: const Text("Detalles",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w900,
                  fontSize: 30)),
          actions: [
            IconButton(
                onPressed: () {},
                icon: const Icon(Icons.notifications,
                    size: 30, color: Colors.white))
          ]),
      backgroundColor: const Color(0x001f1f1f),
      body: const SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: ImagePokemon()),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: AboutPokemon(),
            ),
            SizedBox(height: 10),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Stats(),
            ),
            SizedBox(height: 10),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: AttackPokemon(),
            ),
            SizedBox(height: 10),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: EvolutionPokemon(),
            )
          ],
        ),
      ),
      //  bottomNavigationBar: TabBarWidget2(),
    );
  }
}
