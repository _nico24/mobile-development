import 'package:flutter/material.dart';

class Stats extends StatelessWidget {
  const Stats({super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(20),
              topRight: Radius.circular(20),
              bottomLeft: Radius.circular(20),
              topLeft: Radius.circular(20)),
          side: BorderSide(width: 1)),
      color: const Color(0xFF1F1F1F),
      child: Column(
        children: [
          const ListTile(
            title: Text(
              'Status',
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 23,
              ),
            ),
          ),
          const SizedBox(height: 5),
          _buildStatContainer('HP', Icons.favorite, Colors.red, 50),
          const SizedBox(height: 20),
          _buildStatContainer('ATK', Icons.flash_on, Colors.blue, 75),
          const SizedBox(height: 20),
          _buildStatContainer('DEF', Icons.security, Colors.green, 30),
          const SizedBox(height: 20),
          _buildStatContainer('STAK', Icons.star, Colors.yellow, 90),
          const SizedBox(height: 20),
          _buildStatContainer('SDEF', Icons.shield, Colors.orange, 10),
          const SizedBox(height: 20),
          _buildStatContainer('SPD', Icons.speed, Colors.purple, 60),
          const SizedBox(height: 20),
        ],
      ),
    );
  }

  Widget _buildStatContainer(
      String label, IconData icon, Color color, int value) {
    return SizedBox(
      height: 35,
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Icon(
              icon,
              color: color,
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.only(left: 1),
                child: Text(
                  label,
                  style: const TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 5,
            child: Align(
              alignment: Alignment.centerLeft,
              child: Container(
                decoration: BoxDecoration(
                  color: color,
                  borderRadius: BorderRadius.circular(20),
                ),
                height: double.infinity,
                margin: const EdgeInsets.symmetric(vertical: 4, horizontal: 15),
                child: Center(
                  child: Text(
                    value.toString(), //! Valor de la estadística
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
