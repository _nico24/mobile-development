import 'package:flutter/material.dart';

class CustomAppBar2 extends StatelessWidget {
  const CustomAppBar2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      bottom: false,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Container(
          margin: const EdgeInsets.only(top: 10),
          width: double.infinity,
          child: Row(
            children: <Widget>[
              Image.asset(
                "assets/logo_pokemon.png",
                width: 140,
                height: 50,
              ),
              const Spacer(flex: 10),
              const Icon(
                Icons.notifications,
                size: 30,
                color: Colors.white, // Color blanco para el icono
              ),
            ],
          ),
        ),
      ),
    );
  }
}
