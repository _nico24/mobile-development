export 'package:pokemon_v3/presentation/screens/home/details_pokemon/widgets/evolutions_pokemon.dart';

export 'package:pokemon_v3/presentation/screens/home/details_pokemon/widgets/attack_pokemon.dart';

export 'package:pokemon_v3/presentation/screens/home/details_pokemon/widgets/colors_description.dart';

export 'package:pokemon_v3/presentation/screens/home/details_pokemon/widgets/about_pokemon.dart';

export 'package:pokemon_v3/presentation/screens/home/details_pokemon/widgets/image_pokemon.dart';

export 'package:pokemon_v3/presentation/screens/home/details_pokemon/widgets/custom_appbar2.dart';

export 'package:pokemon_v3/presentation/screens/home/details_pokemon/widgets/stats.dart';
