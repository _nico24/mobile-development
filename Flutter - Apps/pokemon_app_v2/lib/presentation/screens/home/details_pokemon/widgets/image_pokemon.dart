import 'package:flutter/material.dart';

class ImagePokemon extends StatelessWidget {
  const ImagePokemon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 310,
      decoration: BoxDecoration(
          color: Colors.black, borderRadius: BorderRadius.circular(20)),
      child: Column(
        children: <Widget>[_Image()],
      ),
    );
  }
}

class _Image extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(30),
      child: Stack(
        alignment: Alignment.center, // Centra los elementos en el Stack
        children: <Widget>[
          _BorderCircle(),
          _BorderCenter(),
          Image.asset(
            "assets/squirtle.png",
            width: double.infinity, // Ajusta el ancho de la imagen
            height: 180, // Ajusta la altura de la imagen
          ),
        ],
      ),
    );
  }
}

class _BorderCircle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 250,
        height: 240,
        decoration: BoxDecoration(
            color: const Color(0xFF1F1F1F),
            borderRadius: BorderRadius.circular(150)),
      ),
    );
  }
}

class _BorderCenter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 70,
        height: 70,
        decoration: BoxDecoration(
            color: Colors.black, borderRadius: BorderRadius.circular(150)),
      ),
    );
  }
}
