import 'package:flutter/material.dart';
import 'package:pokemon_v3/presentation/screens/home/details_pokemon/widgets/widgets.dart';

class AttackPokemon extends StatelessWidget {
  const AttackPokemon({super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(20),
          topLeft: Radius.circular(20),
          bottomRight: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
        side: BorderSide(width: 1),
      ),
      color: const Color(0xFF1F1F1F),
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              "Ataques",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 23),
            ),
            const SizedBox(height: 15),
            const Text(
              "Ataque 1",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 15),
            ),
            const SizedBox(height: 15),
            const Row(
              children: [
                ColorsDescription(type: "poison"),
                SizedBox(width: 10),
                ColorsDescription(type: "psychic")
              ],
            ),
            const SizedBox(height: 20),
            Container(
              height: 1,
              color: Colors.grey,
            ),
            const SizedBox(height: 20),
            const Text(
              "Ataque 2",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 15),
            ),
            const SizedBox(height: 15),
            const Row(
              children: [
                ColorsDescription(type: "grass"),
                SizedBox(width: 10),
                ColorsDescription(type: "ground")
              ],
            ),
            const SizedBox(height: 20),
            Container(
              height: 1,
              color: Colors.grey,
            ),
            const SizedBox(height: 20),
            const Text(
              "Ataque 3",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 15),
            ),
            const SizedBox(height: 15),
            const Row(
              children: [
                ColorsDescription(type: "ice"),
                SizedBox(width: 10),
                ColorsDescription(type: "rock")
              ],
            ),
            const SizedBox(height: 20),
            Container(
              height: 1,
              color: Colors.grey,
            ),
            const SizedBox(height: 20),
            const Text(
              "Ataque 4",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 15),
            ),
            const SizedBox(height: 15),
            const Row(
              children: [
                ColorsDescription(type: "water"),
                SizedBox(width: 10),
                ColorsDescription(type: "bug")
              ],
            ),
            const SizedBox(height: 7)
          ],
        ),
      ),
    );
  }
}
