import 'package:flutter/material.dart';

class ColorsDescription extends StatelessWidget {
  final String type;

  const ColorsDescription({super.key, required this.type});

  Color getTypeColor(String type) {
    switch (type) {
      case 'normal':
        return const Color(0xFFbdbeb0);
      case 'poison':
        return const Color(0xFF995E98);
      case 'psychic':
        return const Color(0xFFE96EB0);
      case 'grass':
        return const Color(0xFF9CD363);
      case 'ground':
        return const Color(0xFFE3C969);
      case 'ice':
        return const Color(0xFFAFF4FD);
      case 'fire':
        return const Color(0xFFE7614D);
      case 'rock':
        return const Color(0xFFCBBD7C);
      case 'dragon':
        return const Color(0xFF8475F7);
      case 'water':
        return const Color(0xFF6DACF8);
      case 'bug':
        return const Color(0xFFC5D24A);
      case 'dark':
        return const Color(0xFF886958);
      case 'fighting':
        return const Color(0xFF9E5A4A);
      case 'ghost':
        return const Color(0xFF7774CF);
      case 'steel':
        return const Color(0xFFC3C3D9);
      case 'flying':
        return const Color(0xFF81A2F8);
      case 'fairy':
        return const Color(0xFFEEB0FA);
      default:
        return Colors.black;
    }
  }

  @override
  Widget build(BuildContext context) {
    Color color = getTypeColor(type);

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 1),
      child: Container(
        padding: const EdgeInsets.all(4),
        decoration: BoxDecoration(
          color: color,
          borderRadius: const BorderRadius.all(Radius.circular(18)),
        ),
        child: Row(
          children: [
            const Icon(
              Icons.sports_mma,
              color: Colors.black,
            ),
            Text(
              type.toUpperCase(), //! Mostrar el tipo en mayúsculas
              style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
