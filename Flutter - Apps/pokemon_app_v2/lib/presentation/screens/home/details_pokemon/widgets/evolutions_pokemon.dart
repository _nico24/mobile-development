import 'package:flutter/material.dart';
import 'package:pokemon_v3/presentation/screens/home/details_pokemon/widgets/colors_description.dart';

class EvolutionPokemon extends StatelessWidget {
  const EvolutionPokemon({super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 5,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(20),
            topLeft: Radius.circular(20),
            bottomRight: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
          side: BorderSide(width: 1),
        ),
        color: const Color(0xFF1F1F1F),
        child: Padding(
            padding: const EdgeInsets.all(16),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              const Text(
                "Evoluciones",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 23),
              ),
              const SizedBox(height: 10),
              Row(
                children: [
                  Expanded(
                    child: SizedBox(
                      width: 150, // Ancho deseado para la carta
                      child: Card(
                        elevation: 5,
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            topLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                            topRight: Radius.circular(20),
                          ),
                          side: BorderSide(width: 1, color: Colors.lightBlue),
                        ),
                        color: const Color(0xFF1F1F1F),
                        child: Column(
                          children: [
                            const SizedBox(height: 15),
                            Image.asset(
                              "assets/pokemon1.png",
                              width: 150,
                              height: 150,
                            ),
                            const Text(
                              "Data",
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                              ),
                            ),
                            const SizedBox(height: 10),
                            const Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Center(child: ColorsDescription(type: "water")),
                              ],
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(width: 2),
                  Expanded(
                    child: SizedBox(
                      width: 150, // Ancho deseado para la carta
                      child: Card(
                        elevation: 5,
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            topLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                            topRight: Radius.circular(20),
                          ),
                          side: BorderSide(width: 1, color: Colors.lightBlue),
                        ),
                        color: const Color(0xFF1F1F1F),
                        child: Column(
                          children: [
                            const SizedBox(height: 15),
                            Image.asset(
                              "assets/pokemon2.png",
                              width: 150,
                              height: 150,
                            ),
                            const Text(
                              "Data",
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                              ),
                            ),
                            const SizedBox(height: 10),
                            const Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Center(child: ColorsDescription(type: "fire")),
                              ],
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(width: 2),
                  Expanded(
                    child: SizedBox(
                      width: 150, // Ancho deseado para la carta
                      child: Card(
                        elevation: 5,
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            topLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                            topRight: Radius.circular(20),
                          ),
                          side: BorderSide(width: 1, color: Colors.lightBlue),
                        ),
                        color: const Color(0xFF1F1F1F),
                        child: Column(
                          children: [
                            const SizedBox(height: 15),
                            Image.asset(
                              "assets/pokemon3.png",
                              width: 150,
                              height: 150,
                            ),
                            const Text(
                              "Data",
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                              ),
                            ),
                            const SizedBox(height: 10),
                            const Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Center(
                                    child: ColorsDescription(type: "fighting")),
                              ],
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ])));
  }
}
