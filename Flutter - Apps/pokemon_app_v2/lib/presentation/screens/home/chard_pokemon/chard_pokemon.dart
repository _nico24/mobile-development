import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:pokemon_v3/models/models.dart';
import 'package:pokemon_v3/models/pokemon_description.dart';

import '../../../../api/api.dart';

class ChardPokemon extends StatefulWidget {
  const ChardPokemon({super.key});

  @override
  State<ChardPokemon> createState() => _PokemonChardState();
}

class _PokemonChardState extends State<ChardPokemon> {
  AllPokemons? allpokemon;
  Map<String, String> imageUrls = {}; // Mapa para almacenar las URL de imágenes
  Map<String, int> pokemonIds = {}; // Mapa para almacenar los IDs de los Pokémon

  @override
  void initState() {
    super.initState();
    loadAllPokemon();
  }

  Future<void> loadAllPokemon() async {
    allpokemon = await PokemonResponse.fetchAllPokemon();
    await loadImagesAndIds(); // Cargar imágenes y IDs una vez
    setState(() {});
  }

  Future<void> loadImagesAndIds() async {
    for (var result in allpokemon!.results) {
      try {
        Pokemon pokemon =
            await PokemonResponse.fetchPokemonDetails(result.name);
        imageUrls[result.name] = pokemon.sprites.other!.dreamWorld.frontDefault;
        pokemonIds[result.name] = pokemon.id;
      } catch (e) {
        continue;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return allpokemon == null
        ? Center(heightFactor: MediaQuery.of(context).size.height * 0.01, child: const CircularProgressIndicator())
        : Expanded(
            child: Container(
              margin: const EdgeInsets.only(top: 10),
              child: GridView.builder(
                scrollDirection: Axis.vertical,
                padding: const EdgeInsets.all(0),
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                ),
                itemCount: allpokemon!.results.length,
                itemBuilder: (context, index) {
                  var result = allpokemon?.results[index];
                  var imageUrl = imageUrls[result!.name] ?? '';
                  var idPokemon = pokemonIds[result.name] ?? 0;
                  return _ItemPokemon(
                    pokemonResult: result,
                    imageUrl: imageUrl,
                    idPokemon: idPokemon,
                  );
                },
              ),
            ),
          );
  }
}

class _ItemPokemon extends StatelessWidget {
  const _ItemPokemon({
    required this.pokemonResult,
    required this.imageUrl,
    required this.idPokemon,
  });

  final Result pokemonResult;
  final String imageUrl;
  final int idPokemon;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, '/details_pokemon');
        // Redirige a la página '/detalle'
      },
      child: Card(
        elevation: 5,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          side: BorderSide(width: 1),
        ),
        color: const Color(0xFF1F1F1F),
        child: Padding(
          padding: const EdgeInsets.only(left: 10, top: 5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(pokemonResult.name,
                style: const TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontFamily: 'Helvetica',
                ),
              ),
              Text("ID: $idPokemon", style: const TextStyle(color: Colors.white, fontSize: 13, fontWeight: FontWeight.bold)),
              imageUrl.isEmpty
               ? Image.asset("assets/pokemon1.png", fit: BoxFit.cover)
               : Align(
                alignment: Alignment.center,
                child: SvgPicture.network(imageUrl, height: MediaQuery.of(context).size.height * 0.08),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
