import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:pokemon_v3/presentation/screens/profile/widgets/widgets.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final double coverHeight = 280;
  final double profileHeight = 144;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: ListView(padding: EdgeInsets.zero, children: <Widget>[
        buildTop(),
        buildContent(),
      ]),
    );
  }

  Widget buildCoverImage() => Container(
        color: Colors.grey,
        child: Image.asset(
          "assets/background.gif",
          width: double.infinity,
          height: coverHeight,
          fit: BoxFit.cover,
        ),
      );

  Widget buildProfileImage() => Stack(children: [
        const CircleAvatar(
          radius: 80,
          backgroundColor: Colors.white,
        ),
        Positioned(
          left: 8,
          top: 8,
          child: CircleAvatar(
            radius: profileHeight / 2,
            backgroundImage: const AssetImage("assets/avatar.png"),
          ),
        ),
      ]);

  Widget buildTop() {
    final bottom = profileHeight / 2;
    final top = coverHeight - profileHeight / 2;
    return Stack(
        clipBehavior: Clip.none,
        alignment: Alignment.center,
        children: [
          Container(
              margin: EdgeInsets.only(bottom: bottom),
              child: buildCoverImage()),
          Positioned(top: top, child: buildProfileImage()),
        ]);
  }

  Widget buildContent() => Container(
        padding: const EdgeInsets.symmetric(horizontal: 48),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(height: 20),
            const Text(
              "Nicolas Alexis",
              style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.w800,
                height: 1.4,
                color: Colors.white,
              ),
            ),
            const SizedBox(height: 5),
            const Text(
              "Entrenador Pokemon",
              style: TextStyle(
                fontSize: 20,
                height: 1.4,
                color: Colors.white,
              ),
            ),
            const SizedBox(height: 18),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                buildSocialIcon(
                  FontAwesomeIcons.slack,
                ),
                const SizedBox(width: 12),
                buildSocialIcon(FontAwesomeIcons.github),
                const SizedBox(width: 12),
                buildSocialIcon(FontAwesomeIcons.twitter),
                const SizedBox(width: 12),
                buildSocialIcon(FontAwesomeIcons.linkedin),
              ],
            ),
            const SizedBox(height: 16),
            const Divider(
              color: Colors.white,
            ),
            const SizedBox(height: 16),
            const NumbersWidget(),
            const SizedBox(height: 25),
            const Divider(
              color: Colors.white,
            ),
            const Row(
              children: [
                Text(
                  "Sobre mi",
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.w800,
                    height: 1.4,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
            const SizedBox(height: 16),
            const Text(
              "Saludos, soy un apasionado entrenador de Pokémon que vive en un mundo donde la aventura y la estrategia se entrelazan en emocionantes batallas. Desde que era un niño, he explorado las regiones de Kanto, Johto, Hoenn, Sinnoh, Unova, Kalos, Alola y Galar, siempre en busca de nuevos desafíos y la oportunidad de fortalecer el vínculo con mis adorables compañeros Pokémon. Mi Pokédex es un tesoro de conocimiento, y he cultivado una profunda comprensión de los tipos, las habilidades y las evoluciones de mis amigos Pokémon. No solo me dedico a capturarlos, sino que también invierto tiempo en entrenarlos con paciencia y cariño para que alcancen su máximo potencial. La estrategia en batalla es mi sello distintivo, y estoy constantemente buscando movimientos y combinaciones perfectas para enfrentar a otros entrenadores con honor y deportividad. La camaradería que se crea en el mundo Pokémon es única. Me encanta intercambiar experiencias y consejos con otros entrenadores, y no hay nada más emocionante que enfrentarme a un rival digno en el campo de batalla. Desde los legendarios hasta los pequeños y simpáticos Pokémon de tipo planta, todos tienen un lugar especial en mi equipo y en mi corazón. Fuera de las batallas, me encanta explorar los paisajes impresionantes de cada región, ayudar a los Pokémon en apuros y conocer las historias únicas de cada pueblo y ciudad. Además, siempre estoy atento a las novedades sobre eventos y torneos Pokémon, ya que me gusta poner a prueba mis habilidades en la escena competitiva. Ser un jugador de Pokémon no es solo un hobby, es un estilo de vida lleno de emoción, amistad y aventura. Listo para enfrentar cualquier desafío que el mundo Pokémon tenga reservado y hacer nuevos amigos en el camino.",
              style: TextStyle(
                fontSize: 18,
                height: 1.4,
                color: Colors.white,
              ),
              textAlign: TextAlign.justify,
            ),
            const Divider(),
            const SizedBox(
              height: 5,
            )
          ],
        ),
      );

  Widget buildSocialIcon(IconData icon) => CircleAvatar(
        radius: 25,
        child: Material(
          shape: const CircleBorder(),
          clipBehavior: Clip.hardEdge,
          color: Colors.transparent,
          child: InkWell(
            onTap: () {},
            child: Center(
              child: Icon(icon, size: 32),
            ),
          ),
        ),
      );
}
