import 'package:flutter/material.dart';

class NumbersWidget extends StatelessWidget {
  const NumbersWidget({super.key});

  @override
  Widget build(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          buildButton(text: "Proyectos", value: 39),
          const Text(
            "|",
            style: TextStyle(color: Colors.white, fontSize: 30),
          ),
          buildDivider(),
          buildButton(text: "Siguiendo", value: 529),
          const Text(
            "|",
            style: TextStyle(color: Colors.white, fontSize: 30),
          ),
          buildDivider(),
          buildButton(text: "Seguidores", value: 584),
          buildDivider(),
        ],
      );

  Widget buildDivider() => const SizedBox(
        child: SizedBox(width: 5),
      );

  Widget buildButton({required String text, required int value}) =>
      MaterialButton(
        padding: const EdgeInsets.symmetric(vertical: 4),
        onPressed: () {},
        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "$value",
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                color: Colors.white,
              ),
            ),
            const SizedBox(height: 2),
            Text(
              text,
              style: const TextStyle(
                fontSize: 16,
                color: Colors.white,
              ),
            ),
          ],
        ),
      );
}
