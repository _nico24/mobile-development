export 'package:pokemon_v3/presentation/screens/favorites/favorite_page.dart';
export 'package:pokemon_v3/presentation/screens/profile/profile_page.dart';
export 'package:pokemon_v3/presentation/screens/settings/settings_page.dart';
export 'package:pokemon_v3/presentation/screens/main_menu/main_menu.dart';
export 'package:pokemon_v3/presentation/screens/home/details_pokemon/details_pokemon.dart';
