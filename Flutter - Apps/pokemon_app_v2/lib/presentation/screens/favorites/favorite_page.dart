import 'package:flutter/material.dart';

class FavoritePage extends StatelessWidget {
  const FavoritePage({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: Column(children: [
          Text(
            "Favorite Page",
            style: TextStyle(color: Colors.white),
          ),
        ]),
      ),
    );
  }
}
