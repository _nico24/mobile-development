import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pokemon_v3/bloc/blocs.dart';
import 'package:pokemon_v3/presentation/screens/screens.dart';

import '../bloc/bottom_navigation/bottom_navigation_bloc.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<PokemonsBloc>(create: (context) => PokemonsBloc()),
        BlocProvider<BottomNavigationBloc>(
            create: (_) => BottomNavigationBloc()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Material App',
        initialRoute: '/',
        routes: {
          '/': (context) => const MainMenu(),
          '/details_pokemon': (context) => const DetailsPokemon(),
        },
      ),
    );
  }
}
