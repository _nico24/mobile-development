import 'dart:convert';

AllPokemons pokemonListResultFromJson(String str) =>
    AllPokemons.fromJson(json.decode(str));

String pokemonListResultToJson(AllPokemons data) => json.encode(data.toJson());

class AllPokemons {
  final int count;
  final dynamic next;
  final dynamic previous;
  final List<Result> results;

  AllPokemons({
    required this.count,
    this.next,
    this.previous,
    required this.results,
  });

  factory AllPokemons.fromJson(Map<String, dynamic> json) => AllPokemons(
        count: json["count"],
        next: json["next"],
        previous: json["previous"],
        results:
            List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "next": next,
        "previous": previous,
        "results": List<dynamic>.from(results.map((x) => x.toJson())),
      };
}

class Result {
  final String name;
  final String url;

  Result({
    required this.name,
    required this.url,
  });

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        name: json["name"],
        url: json["url"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "url": url,
      };
}
