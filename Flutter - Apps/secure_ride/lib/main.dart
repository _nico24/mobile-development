import 'package:secure_ride/pages/home/home_page.dart';
import 'package:secure_ride/pages/login/login_page.dart';
import 'package:secure_ride/pages/register/register_page.dart';
import 'package:secure_ride/utils/colors.dart' as custom_colors;
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart'; // Importa Firebase Core

void main() async {
  // Inicializa Firebase antes de ejecutar la aplicación
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: const FirebaseOptions(
      apiKey: "AIzaSyCnKZC5jtt4rrPc5LeN-Hg4JaVOYIIc4QI",
      appId: "1:811872222814:android:9b405774ba66881cf37ecf",
      messagingSenderId: "811872222814",
      projectId: "ride-65a08",
      )
  );
  
  runApp(const Main());
}

class Main extends StatelessWidget {
  const Main({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Uber App',
      initialRoute: "home",
      theme: ThemeData(
        fontFamily: "NimbusSans",
        appBarTheme: const AppBarTheme(
          elevation: 0, 
          backgroundColor: custom_colors.Colors.secondaryColor,
          foregroundColor: custom_colors.Colors.primaryColor,
          surfaceTintColor: custom_colors.Colors.secondaryColor,
        ),
      ),
      routes: {
        "home" : (BuildContext context) => const HomePage(),
        "login" : (BuildContext context) => const LoginPage(),
        "register": (BuildContext context) => const RegisterPage(),
      },
    );
  }
}
