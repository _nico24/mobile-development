import 'package:flutter/material.dart';
import 'package:secure_ride/utils/colors.dart' as custom_colors;


class ButtonApp extends StatelessWidget {
  const ButtonApp({
    super.key, 
    this.color = custom_colors.Colors.secondaryColor,
    this.textColor = custom_colors.Colors.primaryColor,
    this.icon = Icons.arrow_forward_ios,
    required this.onPressed,
    required this.text,
  });

  final Color color;
  final Color textColor;
  final String text;
  final IconData icon;
  final Function onPressed;


  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        onPressed();
      },
      style: ElevatedButton.styleFrom(
        foregroundColor: textColor, backgroundColor: color,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      child: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: Container(
              height: 50,
              alignment: Alignment.center,
              child: Text(
                text,
                style: const TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: SizedBox(
              height: 50,
              child: CircleAvatar(
                radius: 15,
                backgroundColor: custom_colors.Colors.primaryColor,
                child: Icon(
                  icon,
                  color: custom_colors.Colors.secondaryColor,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
