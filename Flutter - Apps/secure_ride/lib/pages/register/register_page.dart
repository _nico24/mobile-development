import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:secure_ride/pages/register/register_controller.dart';
import 'package:secure_ride/utils/colors.dart' as custom_colors;

import '../../widgets/button_app.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  late RegisterController registerController;

  @override
  void initState() {
    super.initState();
    registerController = RegisterController();
    initLoginController();
  }

  Future<void> initLoginController() async {
    print("Init State");
    await registerController.init(context);
    print("Metodo Schedule");
  }

  @override
  Widget build(BuildContext context) {
    print("Metodo Build");
    return Scaffold(
      key: registerController.key,
      appBar: AppBar(
        backgroundColor: custom_colors.Colors.secondaryColor,
      ),
      body:  SingleChildScrollView(
        child: Column(
          children: [
            const _BannerApp(),
            const SizedBox(height: 10),
            const _TextRegister(),
            _TextUserName(registerController: registerController),
            const SizedBox(height: 10),
            _TextFieldEmail(registerController: registerController,),
            const SizedBox(height: 10),
            _TextFieldPassword(registerController: registerController,),
            const SizedBox(height: 10),
            _TextFieldConfirmPassword(registerController: registerController,),
            _ButtonRegister(registerController: registerController,),
          ],
        ),
      )
    );
  }
}

class _BannerApp extends StatelessWidget {
  const _BannerApp();

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: WaveClipperTwo(),
      child: Container(
        color: custom_colors.Colors.secondaryColor,
        height: MediaQuery.of(context).size.height * 0.22,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset(
              "assets/img/logo_app.png",
              width: 150,
              height: 100,
            ),
            const Text(
              "Facil y Rapido",
              style: TextStyle(fontFamily: "Pacifico", fontSize: 22, color: custom_colors.Colors.primaryColor, fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );
  }
}


class _TextRegister extends StatelessWidget {
  const _TextRegister();

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      margin: const EdgeInsets.symmetric(horizontal: 30),
      child: const Text("Registro",
      style: TextStyle(
        color: custom_colors.Colors.secondaryColor,
        fontWeight: FontWeight.bold,
        fontSize: 25
      ),),
    );
  }
}

class _TextFieldEmail extends StatelessWidget {
  final RegisterController registerController;
  const _TextFieldEmail({
    required this.registerController,
    
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 30),
      child:  TextField(
        controller: registerController.emailController,
        style: const TextStyle(color: custom_colors.Colors.secondaryColor),
        decoration: const InputDecoration(
          hintText: "correo@gmail.com",
          hintStyle: TextStyle(color: custom_colors.Colors.secondaryColor),
          labelText: "Correo electronico",
          labelStyle: TextStyle(color: custom_colors.Colors.secondaryColor),
          suffixIcon: Icon(Icons.email_outlined,
          color: custom_colors.Colors.secondaryColor,
          )
        ),
      ),
    );
  }
}

class _TextUserName extends StatelessWidget {
  final RegisterController registerController;
  const _TextUserName({
    required this.registerController,
    
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child:  TextField(
        controller: registerController.userNameController,
        style: const TextStyle(color: custom_colors.Colors.secondaryColor),
        decoration: const InputDecoration(
          hintText: "Nicolas Alexis",
          hintStyle: TextStyle(color: custom_colors.Colors.secondaryColor),
          labelText: "Nombre de Usuario",
          labelStyle: TextStyle(color: custom_colors.Colors.secondaryColor),
          suffixIcon: Icon(Icons.person_outline,
          color: custom_colors.Colors.secondaryColor,
          )
        ),
      ),
    );
  }
}

class _TextFieldPassword extends StatelessWidget {
  final RegisterController registerController;
  const _TextFieldPassword({required this.registerController});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: TextField(
        controller: registerController.passwordController,
        obscureText: true,
        style: const TextStyle(color: custom_colors.Colors.secondaryColor),
        decoration: const InputDecoration(
          labelText: "Contraseña",
          labelStyle: TextStyle(color: custom_colors.Colors.secondaryColor),
          suffixIcon: Icon(Icons.lock_open_outlined,
          color: custom_colors.Colors.secondaryColor,
          )
        ),
      ),
    );
  }
}

class _TextFieldConfirmPassword extends StatelessWidget {
  final RegisterController registerController;
  const _TextFieldConfirmPassword({required this.registerController});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: TextField(
        controller: registerController.confirmPasswordController,
        obscureText: true,
        style: const TextStyle(color: custom_colors.Colors.secondaryColor),
        decoration: const InputDecoration(
          labelText: "Confirmar Contraseña",
          labelStyle: TextStyle(color: custom_colors.Colors.secondaryColor),
          suffixIcon: Icon(Icons.lock_open_outlined,
          color: custom_colors.Colors.secondaryColor,
          )
        ),
      ),
    );
  }
}

class _ButtonRegister extends StatelessWidget {
  final RegisterController registerController;
  const _ButtonRegister({required this.registerController});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 30, vertical: 25),
      child: ButtonApp(
        onPressed: registerController.register, 
        text: 'Registrar ahora',),
    );
  }
}
