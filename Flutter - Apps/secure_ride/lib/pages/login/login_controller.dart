import 'package:flutter/material.dart';
import 'package:progress_dialog_null_safe/progress_dialog_null_safe.dart';
import 'package:secure_ride/providers/auth_provider.dart';
import 'package:secure_ride/utils/my_progress_dialog.dart';
import 'package:secure_ride/utils/snackbar.dart' as utils;

class LoginController {
  late BuildContext context;
  GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
  late TextEditingController emailController = TextEditingController();
  late TextEditingController passwordController = TextEditingController();
  late AuthProvider authProvider = AuthProvider();
  late ProgressDialog progressDialog;

  Future<void> init(BuildContext context) async {
    this.context = context;
    authProvider = AuthProvider();
    progressDialog = MyProgressDialog.createProgressDialog(context, "Espere un momento...");
  }

  static goToRegisterPage(BuildContext context){
    Navigator.pushNamed(context, "register");
  }

  Future<void> login() async {
    String email = emailController.text.trim();
    String password = passwordController.text.trim();

    print("Email: $email");
    print("Password: $password");

    progressDialog.show();

    try {
      bool isLogin = await authProvider.login(email, password);
    
      if (isLogin) {
        progressDialog.hide();
        print("El usuario esta logeado");
        utils.Snackbar.showSnackbar(context, key, "El usuario esta logeado");
      } else {
        progressDialog.hide();
        utils.Snackbar.showSnackbar(context, key, "El usuario no se pudo autenticar");
        print("El usuario no se pudo autenticar");
      }

    } catch (error) {
      utils.Snackbar.showSnackbar(context, key, "Error: $error");
      progressDialog.hide();
      print("Error: $error");
    }
  }
}
