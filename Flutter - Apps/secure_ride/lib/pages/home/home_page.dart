import 'package:secure_ride/pages/home/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:secure_ride/utils/colors.dart' as custom_colors;


class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topRight, 
                end: Alignment.bottomLeft, 
                colors: [custom_colors.Colors.secondaryColor, Colors.black87]),
                ),
            child: const Column(
              children: [
                _BannerApp(),
                SizedBox(height: 50),
                _TextSelectRol(),
                SizedBox(height: 30),
                _ImageTypeUser(image: 'assets/img/pasajero.png',),
                SizedBox(height: 10),
                _TextTypeUser(typeUser: "Cliente"),
                SizedBox(height: 30),
                _ImageTypeUser(image: "assets/img/driver.png"),
                SizedBox(height: 20),
                _TextTypeUser(typeUser: 'Conductor'),
                SizedBox(height: 78),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _BannerApp extends StatelessWidget {
  const _BannerApp();

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: DiagonalPathClipperTwo(),
      child: Container(
        color: custom_colors.Colors.primaryColor,
        height: MediaQuery.of(context).size.height * 0.30,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset(
              "assets/img/logo_app.png",
              width: 150,
              height: 100,
            ),
            const Text(
              "Facil y Rapido",
              style: TextStyle(fontFamily: "Pacifico", fontSize: 22, fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );
  }
}

class _TextSelectRol extends StatelessWidget {
  const _TextSelectRol();

  @override
  Widget build(BuildContext context) {
    return const Text(
      "SELECCIONA TU ROL",
      style: TextStyle(color: custom_colors.Colors.primaryColor, fontFamily: "OneDay", fontSize: 20, fontWeight: FontWeight.bold),
    );
  }
}

class _ImageTypeUser extends StatelessWidget {
  const _ImageTypeUser({
    required this.image,
  });

  final String image;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        HomeController.goToLoginPage(context);
      },
      child: CircleAvatar(
        backgroundImage: AssetImage(image),
        radius: 50,
        backgroundColor: Colors.grey[900],
      ),
    );
  }

  
}

class _TextTypeUser extends StatelessWidget {
  const _TextTypeUser({
    required this.typeUser,
  });

  final String typeUser;

  @override
  Widget build(BuildContext context) {
    return Text(
      typeUser,
      style: const TextStyle(color: custom_colors.Colors.primaryColor, fontSize: 16),
    );
  }
}
