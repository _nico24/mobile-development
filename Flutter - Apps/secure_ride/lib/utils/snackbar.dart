// ignore_for_file: unnecessary_null_comparison

import 'package:flutter/material.dart';
import 'package:secure_ride/utils/colors.dart' as custom_colors;

class Snackbar {
  static void showSnackbar(BuildContext context, GlobalKey<ScaffoldState> key, String text){
    if (context == null || key == null || key.currentState == null) return;

    FocusScope.of(context).requestFocus(FocusNode());

    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          text,
          textAlign: TextAlign.center,
          style: const TextStyle(
            color: custom_colors.Colors.primaryColor,
            fontSize: 14
          ),
        ),
        backgroundColor: custom_colors.Colors.secondaryColor,
        duration: const Duration(seconds: 3),
      ),
    );
  }
}
