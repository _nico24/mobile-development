import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:secure_ride/models/client.dart';

class ClientProvider {
  late CollectionReference<Map<String, dynamic>> _collectionReference;

  ClientProvider() {
    _collectionReference =
        FirebaseFirestore.instance.collection("Clients");
  }

  Future<void> create(Client client) async {
    String? errorMessage;

    try {
      await _collectionReference.doc(client.id).set(client.toJson());
    } catch (error) {
      errorMessage = error.toString();
    }

    if (errorMessage != null) {
      throw errorMessage;
    }
  }
}
