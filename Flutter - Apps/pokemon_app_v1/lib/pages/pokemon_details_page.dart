import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:pokemon_app/bloc/pokemon_details_cubit.dart';
import 'package:pokemon_app/reponse/pokemon_details.dart';
import 'package:pokemon_app/widgets/card_widget.dart';
import 'package:pokemon_app/widgets/pokemon_type_view.dart';

import '../widgets/about_pokemon.dart';
import '../widgets/bar_status.dart';
import '../widgets/tab_bar.dart';

class PokemonDetailsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pokedex'),
        backgroundColor: Colors.black,
      ),
      backgroundColor: Colors.black87,
      body: SingleChildScrollView(
        // Envuelve el contenido en un SingleChildScrollView
        child: BlocBuilder<PokemonDetailsCubit, PokemonDetails?>(
          builder: (context, details) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                if (details != null) ...[
                  Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Text(
                        details.name,
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                      margin: EdgeInsets.only(top: 10),
                      width: 200,
                      height: 200,
                      child: Image.network(
                        details.imageUrl,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: AboutWidget(details: details),
                  ),
                ],
                SizedBox(height: 10),
                StatsCard(),
                SizedBox(
                  height: 10,
                ),
                CardWidget(),
                SizedBox(height: 10),
              ],
            );
          },
        ),
      ),
      bottomNavigationBar: TabBarWidget(),
    );
  }
}
