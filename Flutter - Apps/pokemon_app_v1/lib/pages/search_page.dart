import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pokemon_app/bloc/nav_cubit.dart';
import 'package:pokemon_app/bloc/pokemon_bloc.dart';
import 'package:pokemon_app/bloc/pokemon_state.dart';
import 'package:pokemon_app/bloc/pokemon_event.dart';
import 'package:pokemon_app/widgets/custom_appbar.dart';

import '../widgets/tab_bar.dart';

class SearchPage extends StatelessWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pokemonBloc = BlocProvider.of<PokemonBloc>(context);

    return Scaffold(
      backgroundColor: Colors.black,
      body: Column(
        children: [
          const Padding(
            padding: EdgeInsets.all(8.0),
            child: CustomAppBar(texto: "Pokedex"),
          ),
          const SizedBox(height: 10),
          Expanded(
            child: Container(
              constraints: const BoxConstraints(
                minHeight: 200,
                maxHeight: 300,
              ),
              child: Column(
                children: [
                  Container(
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage(
                          "https://static.wikia.nocookie.net/el-senor-electrico/images/3/3b/Pokemon.jpg/revision/latest?cb=20170524172022&path-prefix=es",
                        ),
                        fit: BoxFit.cover,
                      ),
                    ),
                    padding: const EdgeInsets.symmetric(vertical: 30),
                    alignment: Alignment.center,
                    child: Container(
                      margin: const EdgeInsets.symmetric(vertical: 30),
                      child: Card(
                        color: Colors.black,
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.all(10),
                            child: Column(
                              children: [
                                const Text(
                                  'Descubra un nuevo mundo con Pokedex',
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                    // Estilo de fuente personalizado
                                    fontFamily:
                                        'Helvetica', // Cambia 'Arial' por la fuente que desees
                                  ),
                                ),
                                const SizedBox(height: 10),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 15),
                                        child: TextField(
                                          decoration: InputDecoration(
                                            hintText: 'Buscar',
                                            filled: true,
                                            fillColor: Colors.black,
                                            prefixIcon: const Icon(Icons.search),
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                          ),
                                          onChanged: (value) {
                                            // Aquí puedes implementar la lógica de búsqueda
                                          },
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(5),
                                      child: ElevatedButton(
                                        onPressed: () {
                                          // Lógica para el botón "Buscar"
                                        },
                                        child: const Text('Buscar'),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 10),
                  Expanded(
                    child: BlocBuilder<PokemonBloc, PokemonState>(
                      builder: (context, state) {
                        if (state is PokemonLoadInProgress) {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        } else if (state is PokemonPageLoadSuccess) {
                          return GridView.builder(
                            gridDelegate:
                                const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3,
                            ),
                            itemCount: state.pokemonListings.length,
                            itemBuilder: (context, index) {
                              return GestureDetector(
                                onTap: () => BlocProvider.of<NavCubit>(context)
                                    .showPokemonDetails(
                                        state.pokemonListings[index].id),
                                child: Card(
                                  color: const Color(0xFF1F1F1F),
                                  child: GridTile(
                                    child: Column(
                                      children: [
                                        Image.network(state
                                            .pokemonListings[index].imageUrl),
                                        Text(
                                          state.pokemonListings[index].name,
                                          style: const TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white,
// Estilo de fuente personalizado
                                            fontFamily:
                                                'Helvetica', // Cambia 'Arial' por la fuente que desees
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          );
                        } else if (state is PokemonPageLoadFailed) {
                          return Center(
                            child: Text(
                              state.error.toString(),
                              style: const TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
// Estilo de fuente personalizado
                                fontFamily:
                                    'Helvetica', // Cambia 'Arial' por la fuente que desees
                              ),
                            ),
                          );
                        } else {
                          return Container();
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: TabBarWidget(),
    );
  }
}
