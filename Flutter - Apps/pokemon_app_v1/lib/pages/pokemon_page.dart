import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pokemon_app/pages/pokemon_details_page.dart';
import 'package:pokemon_app/pages/search_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../bloc/nav_cubit.dart';

class PokemonPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NavCubit, int>(builder: (context, pokemonId) {
      return Navigator(
        pages: [
          if (pokemonId == null) MaterialPage(child: SearchPage()),
          if (pokemonId != null)
            MaterialPage(
              child: PokemonDetailsPage(),
            ),
        ],
        onPopPage: (route, result) {
          BlocProvider.of<NavCubit>(context).popToPokedex();
          return route.didPop(result);
        },
      );
    });
  }
}
