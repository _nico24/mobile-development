import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pokemon_app/bloc/nav_cubit.dart';
import 'package:pokemon_app/bloc/pokemon_bloc.dart';
import 'package:pokemon_app/bloc/pokemon_details_cubit.dart';
import 'package:pokemon_app/bloc/pokemon_event.dart';
import 'package:pokemon_app/pages/pokemon_page.dart';
import 'package:pokemon_app/pages/search_page.dart';
import 'package:pokemon_app/pages/pokemon_details_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pokemonDetailsCubit = PokemonDetailsCubit();

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      theme: ThemeData.dark(),
      initialRoute: '/',
      routes: {
        '/': (context) => MultiBlocProvider(
              providers: [
                BlocProvider(
                  create: (context) =>
                      PokemonBloc()..add(PokemonPageRequest(page: 0)),
                ),
                BlocProvider(
                  create: (context) =>
                      NavCubit(pokemonDetailsCubit: pokemonDetailsCubit),
                ),
                BlocProvider(
                  create: (context) => pokemonDetailsCubit,
                ),
              ],
              child: PokemonPage(),
            ),
        '/search': (context) => SearchPage(),
        '/pokemon_details': (context) => PokemonDetailsPage(),
      },
    );
  }
}
