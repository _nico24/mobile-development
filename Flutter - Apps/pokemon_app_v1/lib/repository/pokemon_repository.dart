import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:pokemon_app/reponse/pokemon_info_response.dart';
import 'package:pokemon_app/reponse/pokemon_page_response.dart';
import 'package:pokemon_app/reponse/pokemon_species_info_response.dart';

class PokemonRepository {
  final baseUrl = "pokeapi.co";
  final client = http.Client();

  Future<PokemonPageResponse> getPokemonPage(int pageIndex) async {
    final queryParameters = {
      "limit": "200",
      "offset": (pageIndex * 200).toString()
    };
    final uri = Uri.https(baseUrl, "/api/v2/pokemon", queryParameters);

    final response = await client.get(uri);
    final json = jsonDecode(response.body);

    return PokemonPageResponse.fromJson(json);
  }

  Future<PokemonInfoResponse> getPokemonInfo(int pokemonId) async {
    final uri = Uri.https(baseUrl, "/api/v2/pokemon/$pokemonId");

    try {
      final response = await client.get(uri);
      final json = jsonDecode(response.body);
      return PokemonInfoResponse.fromJson(json);
    } catch (e) {
      print(e);
      throw Exception('Failed to fetch Pokemon info');
    }
  }

  Future<PokemonSpeciesInfoResponse> getPokemonSpeciesInfo(
      int pokemonId) async {
    final uri = Uri.https(baseUrl, "/api/v2/pokemon-species/$pokemonId");

    try {
      final response = await client.get(uri);
      final json = jsonDecode(response.body);
      return PokemonSpeciesInfoResponse.fromJson(json);
    } catch (e) {
      print(e);
      throw Exception('Failed to fetch Pokemon species info');
    }
  }
}
