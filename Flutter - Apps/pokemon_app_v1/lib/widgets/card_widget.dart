import 'package:flutter/material.dart';

class CardWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      color: Color(0xFF1F1F1F),
      child: Padding(
        padding: EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(bottom: 8.0),
              child: Row(
                children: [
                  Icon(
                    Icons.sports_mma,
                    color: Colors.orange,
                  ),
                  SizedBox(width: 8.0),
                  Text(
                    'Movimientos',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 10),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  buildListItem(
                    'Ataque Normal: ???',
                    'https://media.giphy.com/media/68kKd6gmSKYww/giphy.gif',
                  ),
                  SizedBox(width: 10),
                  buildListItem(
                    'Ataque Especial: ???',
                    'https://media.giphy.com/media/v1.Y2lkPTc5MGI3NjExOTI4MmNmZDQxNjc1MzBmMWEwYzFmZjM3NGZhZmI5MmFiMDU2OTRhOSZlcD12MV9pbnRlcm5hbF9naWZzX2dpZklkJmN0PWc/12r4pHjvAOv48o/giphy.gif',
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildListItem(String title, String imageUrl) {
    return Container(
      width: 180,
      child: Column(
        children: [
          Icon(
            Icons.gavel,
            color: Colors.orange,
          ),
          SizedBox(height: 10),
          Text(
            title,
            style: TextStyle(
              fontSize: 16,
              color: Colors.white,
            ),
          ),
          SizedBox(height: 10),
          ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: Image.network(
              imageUrl,
              width: 150,
              height: 150,
              fit: BoxFit.cover,
            ),
          ),
        ],
      ),
    );
  }
}
