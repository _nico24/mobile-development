import 'package:flutter/material.dart';

class PokemonTypeView extends StatelessWidget {
  final String type;

  PokemonTypeView({required this.type});

  Color getTypeColor(String type) {
    switch (type) {
      case 'normal':
        return Color(0xFFbdbeb0);
      case 'poison':
        return Color(0xFF995E98);
      case 'psychic':
        return Color(0xFFE96EB0);
      case 'grass':
        return Color(0xFF9CD363);
      case 'ground':
        return Color(0xFFE3C969);
      case 'ice':
        return Color(0xFFAFF4FD);
      case 'fire':
        return Color(0xFFE7614D);
      case 'rock':
        return Color(0xFFCBBD7C);
      case 'dragon':
        return Color(0xFF8475F7);
      case 'water':
        return Color(0xFF6DACF8);
      case 'bug':
        return Color(0xFFC5D24A);
      case 'dark':
        return Color(0xFF886958);
      case 'fighting':
        return Color(0xFF9E5A4A);
      case 'ghost':
        return Color(0xFF7774CF);
      case 'steel':
        return Color(0xFFC3C3D9);
      case 'flying':
        return Color(0xFF81A2F8);
      case 'normal':
        return Color(0xFFF9E65E);
      case 'fairy':
        return Color(0xFFEEB0FA);
      default:
        return Colors.black;
    }
  }

  @override
  Widget build(BuildContext context) {
    Color color = getTypeColor(type);

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4),
      child: Container(
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.all(Radius.circular(8)),
        ),
        child: Text(
          type.toUpperCase(),
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
