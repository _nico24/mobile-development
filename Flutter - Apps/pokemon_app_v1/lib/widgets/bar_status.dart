import 'package:flutter/material.dart';

class StatsCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      color: Color(0xFF1F1F1F),
      child: Column(
        children: [
          ListTile(
            title: Text(
              'Stats',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
          ),
          SizedBox(height: 8),
          _buildStatContainer('HP', Icons.favorite, Colors.red, 50),
          SizedBox(height: 20),
          _buildStatContainer('ATK', Icons.flash_on, Colors.blue, 75),
          SizedBox(height: 20),
          _buildStatContainer('DEF', Icons.security, Colors.green, 30),
          SizedBox(height: 20),
          _buildStatContainer('STAK', Icons.star, Colors.yellow, 90),
          SizedBox(height: 20),
          _buildStatContainer('SDEF', Icons.shield, Colors.orange, 10),
          SizedBox(height: 20),
          _buildStatContainer('SPD', Icons.speed, Colors.purple, 60),
          SizedBox(height: 20),
        ],
      ),
    );
  }

  Widget _buildStatContainer(
      String label, IconData icon, Color color, int value) {
    return Container(
      height: 40,
      child: Row(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8),
            child: Icon(
              icon,
              color: color,
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
              padding: EdgeInsets.only(left: 8),
              alignment: Alignment.centerLeft,
              child: Text(
                label,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 5,
            child: Container(
              decoration: BoxDecoration(
                color: color,
                borderRadius: BorderRadius.circular(20),
              ),
              height: double.infinity,
              margin: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
              child: Center(
                child: Text(
                  value.toString(),
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
