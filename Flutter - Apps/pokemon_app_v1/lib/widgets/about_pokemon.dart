import 'package:flutter/material.dart';
import 'package:pokemon_app/reponse/pokemon_details.dart';
import 'package:pokemon_app/widgets/pokemon_type_view.dart';

class AboutWidget extends StatelessWidget {
  final PokemonDetails details;

  const AboutWidget({required this.details});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: const Color(0xFF1F1F1F),
        borderRadius: BorderRadius.circular(10.0),
      ),
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'Sobre',
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 10),
          Text(
            details.description,
          ),
          const SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: details.types
                .map((type) => PokemonTypeView(type: type))
                .toList(),
          ),
        ],
      ),
    );
  }
}
