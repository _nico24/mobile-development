import 'package:flutter/material.dart';
import 'package:pokemon_app/pages/search_page.dart';

class TabBarWidget extends StatefulWidget {
  @override
  _TabBarWidgetState createState() => _TabBarWidgetState();
}

class _TabBarWidgetState extends State<TabBarWidget> {
  int _currentIndex = 0;

  void changeTab(int index) {
    setState(() {
      _currentIndex = index;
    });

    Future.microtask(() {
      // Redirigir a la página correspondiente al índice seleccionado
      switch (_currentIndex) {
        case 0:
          // Implementa la lógica para redirigir a la página de ajustes
          break;
        case 1:
          // Redirigir a la página de búsqueda
          Navigator.pushReplacementNamed(context, '/search');
          break;
        case 2:
          // Implementa la lógica para redirigir a la página de favoritos
          break;
        case 3:
          // Implementa la lógica para redirigir a la página de perfil
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: _currentIndex,
      onTap: changeTab,
      backgroundColor: Colors.black,
      selectedItemColor: Colors.lightBlue,
      unselectedItemColor: Colors.white,
      type: BottomNavigationBarType.fixed,
      items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.settings),
          label: 'Ajustes',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Inicio',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.favorite),
          label: 'Favoritos',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person),
          label: 'Perfil',
        ),
      ],
    );
  }
}
