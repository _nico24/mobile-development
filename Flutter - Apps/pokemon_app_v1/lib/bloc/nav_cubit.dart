import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pokemon_app/bloc/pokemon_details_cubit.dart';

class NavCubit extends Cubit<int> {
  PokemonDetailsCubit? pokemonDetailsCubit;
  NavCubit({this.pokemonDetailsCubit}) : super(0);

  void showPokemonDetails(int pokemonId) {
    pokemonDetailsCubit?.getPokemonDetails(pokemonId);
    emit(pokemonId);
  }

  void popToPokedex() {
    emit(0);
  }
}
