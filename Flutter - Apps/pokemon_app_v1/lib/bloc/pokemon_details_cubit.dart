import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pokemon_app/reponse/pokemon_details.dart';

import 'package:pokemon_app/reponse/pokemon_species_info_response.dart';
import 'package:pokemon_app/repository/pokemon_repository.dart';

import '../reponse/pokemon_info_response.dart';

class PokemonDetailsCubit extends Cubit<PokemonDetails?> {
  final _pokemonRepository = PokemonRepository();
  PokemonDetailsCubit() : super(null);

  void getPokemonDetails(int pokemonId) async {
    final responses = await Future.wait([
      _pokemonRepository.getPokemonInfo(pokemonId),
      _pokemonRepository.getPokemonSpeciesInfo(pokemonId)
    ]);

    final pokemonInfo = responses[0] as PokemonInfoResponse?;
    final speciesInfo = responses[1] as PokemonSpeciesInfoResponse?;

    if (pokemonInfo != null && speciesInfo != null) {
      emit(PokemonDetails(
        id: pokemonInfo.id,
        name: pokemonInfo.name,
        imageUrl: pokemonInfo.imageUrl,
        types: pokemonInfo.types,
        height: pokemonInfo.heigth,
        weight: pokemonInfo.weight,
        description: speciesInfo.description,
      ));
    } else {
      emit(
          null); // Manejar el caso de valores nulos como sea apropiado para tu lógica
    }
  }

  void clearPokemonDetails() => emit(null);
}
