import 'package:flutter/material.dart';

import '../widgets/widgets.dart';

class AudioButton extends StatefulWidget {
  final String audioPath;
  final String buttonText;
  final Color buttonColor;

  const AudioButton({
    Key? key,
    required this.audioPath,
    required this.buttonText,
    required this.buttonColor,
  }) : super(key: key);

  @override
  AudioButtonState createState() => AudioButtonState();
}
