import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

import '../models/models.dart';

class AudioButtonState extends State<AudioButton> {
  late AudioPlayer _player;
  bool _isPlaying = false;

  @override
  void initState() {
    super.initState();
    _player = AudioPlayer();
    _player.onPlayerComplete.listen((event) {
      setState(() {
        _isPlaying = false;
      });
    });
  }

  @override
  void dispose() {
    _player.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        foregroundColor: Colors.black,
        backgroundColor: widget.buttonColor,
      ),
      onPressed: _isPlaying
          ? null
          : () {
              _player.play(AssetSource(widget.audioPath));
              setState(() {
                _isPlaying = true;
              });
            },
      child: Text(widget.buttonText),
    );
  }
}
