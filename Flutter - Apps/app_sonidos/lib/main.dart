import 'package:flutter/material.dart';
import 'package:flutter_app_sonidos/screens/home.dart';

void main() => runApp(const Main());

class Main extends StatelessWidget {
  const Main({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      theme: ThemeData.dark(),
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: const Text('Sonidos Memes'),
        ),
        body: const MyHomePage(),
      ),
    );
  }
}
