import 'package:flutter/material.dart';

import '../models/models.dart';

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: GridView.count(
        crossAxisCount: 3,
        children: const <Widget>[
          AudioButton(
              audioPath: "no-spiderman.mp3",
              buttonText: "No",
              buttonColor: Colors.redAccent),
          AudioButton(
              audioPath: "si-spiderman.mp3",
              buttonText: "Si",
              buttonColor: Colors.purpleAccent),
          AudioButton(
              audioPath: "no-bueno-si.mp3",
              buttonText: "No, bueno si",
              buttonColor: Colors.orangeAccent),
          AudioButton(
              audioPath: "probablemente.mp3",
              buttonText: "Probablemente",
              buttonColor: Colors.amberAccent),
          AudioButton(
              audioPath: "nose.mp3",
              buttonText: "Nosé",
              buttonColor: Colors.greenAccent),
          AudioButton(
              audioPath: "pipipi.mp3",
              buttonText: "Pipipi",
              buttonColor: Colors.indigoAccent),
          AudioButton(
              audioPath: "cargando-mi-daga.mp3",
              buttonText: "Esta cargando mi daga ctmr",
              buttonColor: Colors.cyanAccent),
          AudioButton(
              audioPath: "causa-gaa.mp3",
              buttonText: "Que pasa causa gaa",
              buttonColor: Colors.deepOrangeAccent),
        ],
      ),
    );
  }
}
