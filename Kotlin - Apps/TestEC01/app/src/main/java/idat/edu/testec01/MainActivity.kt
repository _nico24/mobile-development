package idat.edu.testec01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private val retrofit = RetrofitHelper.getInstance()

    private lateinit var txtResponse: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        txtResponse = findViewById(R.id.txtResult)

        lifecycleScope.launch(Dispatchers.IO) {
            Log.d("corutinas","iniciamos la corutina")
            val response : Response<DogResponse> = retrofit.getRamdomDog()
            if (response.isSuccessful) {
                txtResponse.setText(response.body().toString())
            } else {

            }
        }

    }
}