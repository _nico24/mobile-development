package idat.edu.testec01

import com.google.gson.annotations.SerializedName
import retrofit2.Response
import retrofit2.http.GET

interface ApiServices {

    @GET("/api/6459025487508609/search/Goliath")
    suspend fun getRamdomDog() : Response<DogResponse>

}

data class DogResponse(@SerializedName("response") val response: String )