package idat.edu.imcapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var txtPeso: EditText
    private lateinit var txtTalla: EditText
    private lateinit var btnCalcular: Button
    private lateinit var txtResultado: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        iniciarComponentes()
        iniciarUI()
        iniciarEventos()
    }

    private fun iniciarComponentes(){
        txtPeso = findViewById(R.id.txtPesoKg);
        txtTalla = findViewById(R.id.txtTallaCm);
        btnCalcular = findViewById(R.id.btnCalcular);
        txtResultado = findViewById(R.id.txtResultado);
    }

    private fun iniciarUI(){
        txtPeso.text.clear()
        txtTalla.text.clear()
        txtResultado.setText("")
    }

    private fun iniciarEventos(){
        btnCalcular.setOnClickListener {
            calcularIMC()
        }
    }

    private fun calcularIMC(){
        if (txtPeso.text.toString().trim().isEmpty()){
            txtResultado.text = "Debe ingresar el peso"
        } else if (txtTalla.text.toString().trim().isEmpty()) {
            txtResultado.text = "Debe ingresar la talla"
        } else {
            var peso : Double =  txtPeso.text.toString().toDouble()
            var talla : Double =  txtTalla.text.toString().toDouble()
            var resultadoIMC: String

            var tallM: Double = talla / 100
            var imc: Double = peso / (tallM * tallM)

            if (imc <= 18.5) {
                resultadoIMC = " esta por debajo del peso"
            } else if (imc <= 25) {
                resultadoIMC = " esta con peso normal"
            } else if (imc <= 30) {
                resultadoIMC = " esta con sobre peso"
            } else if (imc <= 35) {
                resultadoIMC = " esta con obesidad leve"
            } else if (imc <= 39) {
                resultadoIMC = " esta con obesidad media"
            } else {
                resultadoIMC = " esta con obesidad alta"
            }
            resultadoIMC = "Su IMC es de:$imc$resultadoIMC"
            txtResultado.text = resultadoIMC
        }
    }


}